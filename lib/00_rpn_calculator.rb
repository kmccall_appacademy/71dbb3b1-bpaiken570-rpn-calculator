class RPNCalculator
  attr_accessor :value

  def initialize
    @arr = []
    @value = nil
  end

  def push(num)
    @arr.push(num)
  end

  def plus
    @value = operation(:+)
  end

  def minus
    @value = operation(:-)
  end

  def times
    @value = operation(:*)
  end

  def divide
    @value = operation(:/)
  end

  def operation(operator)
    check
    temp = @arr.pop.to_f
    value = @arr.pop.to_f.send(operator, temp)
    push(value)
    value
  end

  def check
    raise('calculator is empty') if value.nil? && @arr.length < 2 || @arr.empty?
  end

  def tokens(str)
    str.split.map do |e|
      if '123456789'.include?(e)
        e.to_i
      else
        e.to_sym
      end
    end
  end

  def evaluate(str)
    value = nil
    tokens(str).each do |e|
      if e.class == 1.class
        push(e)
      else
        value = operation(e)
      end
    end
    value
  end
end
